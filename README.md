book-api

This is a coding project for an interview, so it has some shortcuts and stuff that would not normally be in a production environment.
Any comment with "side-note" is for noting something added for ease of testing due to permitted shortcuts, or a note about
how it may differ in a production environment because of time limits.

Assume any other comments would be part of the solution in production.

Things I did not get to within the time limit but would include given the time:
Tests for custom JSON converter, linting, handling version checking for MARC21 bulk request when model binding,
seperate validation messages when a MARC21 record mapped to a book is invalid VS when a Book record for a Put request is invalid
(MARC21 bulk post validates the mapped model and not the MARC21 JSON, seemed a little daunting to validate before mapping, not really sure it would be worth it either)