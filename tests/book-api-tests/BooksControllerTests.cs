using Microsoft.VisualStudio.TestTools.UnitTesting;
using book_api.Controllers;
using book_api.Models;
using Microsoft.Extensions.Logging;
using book_api.Services;
using Moq;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using book_api.DataSources;
using System.Collections.Generic;

namespace book_api_tests
{
    [TestClass]
    public class BooksControllerTests
    {
        [TestMethod]
        public async Task GetAll_ReturnsResults()
        {
            // arrange
            Mock<IBookDB> bookDBMock = new Mock<IBookDB>();
            bookDBMock.Setup(bdb => bdb.GetAll())
                .Returns(Task.FromResult<List<Book>>(new List<Book>(){ getValidBook() }));

            BooksController controller = getBooksController(bookDBMock: bookDBMock);

            // act
            List<Book> result = await controller.GetAll();

            // assert
            Assert.AreEqual(result.Count, 1, "should return book result from db");
        }

        [TestMethod]
        public async Task Get_Result_200()
        {
            // arrange
            Mock<IBookDB> bookDBMock = new Mock<IBookDB>();
            bookDBMock.Setup(bdb => bdb.GetBySystemControlNumber(It.IsAny<string>()))
                .Returns(Task.FromResult<Book>(getValidBook()));

            BooksController controller = getBooksController(bookDBMock: bookDBMock);

            // act
            var result = await controller.Get("123");
            OkObjectResult okResult = result as OkObjectResult;

            // assert
            Assert.IsNotNull(okResult, "result was not of the expected type (OkObjectResult)");
            Assert.AreEqual(200, okResult.StatusCode);
            Assert.AreEqual("123", (okResult.Value as Book)?.SystemControlNumber, "should return requested book");
        }

        [TestMethod]
        public async Task Get_NoResult_404()
        {
            // arrange
            Mock<IBookDB> bookDBMock = new Mock<IBookDB>();
            bookDBMock.Setup(bdb => bdb.GetBySystemControlNumber(It.IsAny<string>()))
                .Returns(Task.FromResult<Book>(null));

            BooksController controller = getBooksController(bookDBMock: bookDBMock);

            // act
            var result = await controller.Get("123");
            NotFoundResult notFoundResult = result as NotFoundResult;

            // assert
            Assert.IsNotNull(notFoundResult, "result was not of the expected type (NotFoundResult)");
            Assert.AreEqual(404, notFoundResult.StatusCode);
        }

        [TestMethod]
        public async Task Put_SystemControlNumber_DoesNotMatch_RequestBody_422()
        {
            // no mocks needed, should return before any calls to injected resources
            BooksController controller = getBooksController();

            // act
            var result = await controller.Put("mismatched_identifier", getValidBook());
            BadRequestObjectResult badRequestResult = result as BadRequestObjectResult;

            // assert
            Assert.IsNotNull(badRequestResult, "result was not of the expected type (BadRequestResult)");
            Assert.AreEqual(400, badRequestResult.StatusCode);
        }

        [TestMethod]
        public async Task Put_Insert_201()
        {
            // arrange
            Mock<IBookDB> bookDBMock = new Mock<IBookDB>();
            bookDBMock.Setup(bdb => bdb.GetBySystemControlNumber(It.IsAny<string>()))
                .Returns(Task.FromResult<Book>(null));
            bookDBMock.Setup(bdb => bdb.Insert(It.IsAny<Book>()))
                .Returns(Task.CompletedTask);

            BooksController controller = getBooksController(bookDBMock: bookDBMock);

            // act
            var result = await controller.Put("123", getValidBook());
            CreatedResult createdResult = result as CreatedResult;

            // assert
            Assert.IsNotNull(createdResult, "result was not of the expected type (CreatedResult)");
            Assert.AreEqual(201, createdResult.StatusCode);
            Assert.AreEqual("123", (createdResult.Value as Book)?.SystemControlNumber);
            Assert.AreEqual("123", createdResult.Location);
        }

        [TestMethod]
        public async Task Put_Update_200()
        {
            // arrange
            Mock<IBookDB> bookDBMock = new Mock<IBookDB>();
            bookDBMock.Setup(bdb => bdb.GetBySystemControlNumber(It.IsAny<string>()))
                .Returns(Task.FromResult<Book>(getValidBook()));
            bookDBMock.Setup(bdb => bdb.Update(It.IsAny<Book>()))
                .Returns(Task.CompletedTask);

            BooksController controller = getBooksController(bookDBMock: bookDBMock);

            // act
            var result = await controller.Put("123", getValidBook());
            OkResult okResult = result as OkResult;

            // assert
            Assert.IsNotNull(okResult, "result was not of the expected type (OkResult)");
            Assert.AreEqual(200, okResult.StatusCode);
        }

        [TestMethod]
        public async Task Delete_Deleted_200()
        {
            // arrange
            Mock<IBookDB> bookDBMock = new Mock<IBookDB>();
            bookDBMock.Setup(bdb => bdb.Delete(It.IsAny<string>()))
                .Returns(Task.FromResult<int>(1));

            BooksController controller = getBooksController(bookDBMock: bookDBMock);

            // act
            var result = await controller.Delete("123");
            OkResult okResult = result as OkResult;

            // assert
            Assert.IsNotNull(okResult, "result was not of the expected type (OkResult)");
            Assert.AreEqual(200, okResult.StatusCode);
        }

        [TestMethod]
        public async Task Delete_NotFound_404()
        {
            // arrange
            Mock<IBookDB> bookDBMock = new Mock<IBookDB>();
            bookDBMock.Setup(bdb => bdb.Delete(It.IsAny<string>()))
                .Returns(Task.FromResult<int>(0));

            BooksController controller = getBooksController(bookDBMock: bookDBMock);

            // act
            var result = await controller.Delete("123");
            NotFoundResult notFoundResult = result as NotFoundResult;

            // assert
            Assert.IsNotNull(notFoundResult, "result was not of the expected type (NotFoundResult)");
            Assert.AreEqual(404, notFoundResult.StatusCode);
        }

        public async Task Post_MARC21_Messages_422()
        {
            // arrange
            Mock<IBookService> bookServiceMock = new Mock<IBookService>();
            // if anything goes wrong that would make the request unproocessable, returns early with messages
            MARC21PostResponse marc21resp = new MARC21PostResponse();
            marc21resp.messages = new List<string>() { "ope" };

            bookServiceMock.Setup(bsm => bsm.PostMARC21(It.IsAny<MARC21PostRequest>()))
                .Returns(Task.FromResult<MARC21PostResponse>(marc21resp));

            BooksController controller = getBooksController(bookServiceMock: bookServiceMock);

            // act
            var result = await controller.Post(new MARC21PostRequest());
            UnprocessableEntityObjectResult nopeResult = result as UnprocessableEntityObjectResult;

            // assert
            Assert.IsNotNull(nopeResult, "result was not of the expected type (UnprocessableEntityObjectResult)");
            Assert.AreEqual(422, nopeResult.StatusCode);
            Assert.AreEqual("ope", (nopeResult.Value as MARC21PostResponse)?.messages?[0], "should return messages from book service response");
        }

        private BooksController getBooksController(
            Mock<ILogger<BooksController>> loggerMock = null,
            Mock<IBookDB> bookDBMock = null,
            Mock<IBookService> bookServiceMock = null) 
        {
            BooksController controller = new BooksController(
                (loggerMock != null ? loggerMock.Object : new Mock<ILogger<BooksController>>().Object),
                (bookServiceMock != null ? bookServiceMock.Object : new Mock<IBookService>().Object),
                (bookDBMock != null ? bookDBMock.Object : new Mock<IBookDB>().Object)
            );

            return controller;
        }

        private Book getValidBook()
        {
            Book book = new Book();
            book.SystemControlNumber = "123";
            book.Title = "Unit testing in C#, just the good parts";
            book.PublishedDate = "July 2021";
            book.PublisherName = "Jon Skeet";
            book.FullTextLink = "https://twitter.com/jonskeet/photo";

            return book;
        }
    }
}
