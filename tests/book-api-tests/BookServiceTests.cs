using Microsoft.VisualStudio.TestTools.UnitTesting;
using book_api.Models;
using book_api.Services;
using Moq;
using System.Threading.Tasks;
using book_api.DataSources;

namespace book_api_tests
{
    [TestClass]
    public class BookServiceTests
    {
        [TestMethod]
        public async Task PostMARC21_RecordPositionMissing_ReturnsMessages()
        {
            // arrange
            Mock<IBookDB> bookDBMock = new Mock<IBookDB>();
            bookDBMock.Setup(bdb => bdb.Upsert(It.IsAny<Book>()))
                .Returns(Task.FromResult<bool>(true));

            BookService service = getBookService();

            MARC21PostRequest request = TestDataHelper.getTestMARC21PostRequest();
            request.Records[0].position = "";

            // act
            MARC21PostResponse result = await service.PostMARC21(request);

            // assert
            Assert.AreEqual(result.messages.Count, 1, "should return with a message");
            bookDBMock.Verify(mock => mock.Upsert(It.IsAny<Book>()), Times.Never, "should return early and not process any records");
        }

        [TestMethod]
        public async Task PostMARC21_DuplicateRecordPosition_ReturnsMessages()
        {
            // arrange
            Mock<IBookDB> bookDBMock = new Mock<IBookDB>();
            bookDBMock.Setup(bdb => bdb.Upsert(It.IsAny<Book>()))
                .Returns(Task.FromResult<bool>(true));

            BookService service = getBookService();

            MARC21PostRequest request = TestDataHelper.getTestMARC21PostRequest();
            request.Records[0].position = "0";
            MARC21 secondRecord = new MARC21();
            secondRecord.position = "0";
            request.Records.Add(secondRecord);

            // act
            MARC21PostResponse result = await service.PostMARC21(request);

            // assert
            Assert.AreEqual(result.messages.Count, 1, "should return with a message");
            bookDBMock.Verify(mock => mock.Upsert(It.IsAny<Book>()), Times.Never, "should return early and not process any records");
        }

        [TestMethod]
        public async Task PostMARC21_Record_Invalid()
        {
            // arrange
            Mock<IBookDB> bookDBMock = new Mock<IBookDB>();
            bookDBMock.Setup(bdb => bdb.Upsert(It.IsAny<Book>()))
                .Returns(Task.FromResult<bool>(true));

            BookService service = getBookService(bookDBMock);

            // remove data that would map to book record, request is valid, record is not
            MARC21PostRequest request = TestDataHelper.getTestMARC21PostRequest();
            request.Records[0].datafield.Clear();

            // act
            MARC21PostResponse result = await service.PostMARC21(request);

            // assert
            Assert.AreEqual(result.messages.Count, 0, "should have no messages");
            bookDBMock.Verify(mock => mock.Upsert(It.IsAny<Book>()), Times.Never, "should not process record");
            Assert.IsFalse(result.results[0].success, "should be marked as not sucessfull");
            Assert.IsTrue(result.results[0].messages.Count > 0, "should have validation messages for record");
        }

        [TestMethod]
        public async Task PostMARC21_Success()
        {
            // arrange
            Mock<IBookDB> bookDBMock = new Mock<IBookDB>();
            bookDBMock.Setup(bdb => bdb.Upsert(It.IsAny<Book>()))
                .Returns(Task.FromResult<bool>(true));

            BookService service = getBookService(bookDBMock);

            MARC21PostRequest request = TestDataHelper.getTestMARC21PostRequest();

            // act
            MARC21PostResponse result = await service.PostMARC21(request);

            // assert
            Assert.AreEqual(0, result.messages.Count, "should have no messages");
            bookDBMock.Verify(mock => mock.Upsert(It.IsAny<Book>()), Times.Once, "should process record");
            Assert.IsTrue(result.results[0].success, "should be marked as sucessfull");
            Assert.AreEqual("196889092", result.results[0].systemControlNumber, "should map systemControlNumber to record result in response");
        }

        private BookService getBookService(Mock<IBookDB> bookDBMock = null) 
        {
            BookService service = new BookService(
                (bookDBMock != null ? bookDBMock.Object : new Mock<IBookDB>().Object)
            );

            return service;
        }
    }
}
