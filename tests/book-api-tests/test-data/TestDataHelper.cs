using System;
using System.IO;
using book_api.Models;
using Newtonsoft.Json;

namespace book_api_tests
{
    
    public class TestDataHelper
    {
        public static MARC21PostRequest getTestMARC21PostRequest()
        {
            MARC21PostRequest request;
            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "testMarc21Request.json");
            using (StreamReader r = File.OpenText(filePath))
            {
                string json = r.ReadToEnd();
                request = JsonConvert.DeserializeObject<MARC21PostRequest>(json);
            }

            return request;
        }
    }
}