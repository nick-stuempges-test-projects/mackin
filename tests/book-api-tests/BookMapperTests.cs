using Microsoft.VisualStudio.TestTools.UnitTesting;
using book_api.Models;
using book_api.Mappers;

namespace book_api_tests
{
    [TestClass]
    public class BookMapperTests
    {
        const string NO_INDICATOR = "";

        // this method also tests the varied scenarios of getFieldNRSubFieldValue, no indicator, 1 indicator, 2 indicators, multiple subfields...
        [TestMethod]
        public void BookMapper_mapToBook()
        {
            // arrange
            MARC21PostRequest request = TestDataHelper.getTestMARC21PostRequest();
            MARC21 record = request.Records[0];

            // act
            Book result = BookMapper.mapToBook(record);

            // assert
            Assert.AreEqual(
                BookMapper.getFieldNRSubFieldValue(record, "035", NO_INDICATOR, NO_INDICATOR, "a"),
                result.SystemControlNumber, "SystemControlNumber should be mapped from indicated field and subfield");
            Assert.AreEqual("196889092", result.SystemControlNumber);

            string title = BookMapper.getFieldNRSubFieldValue(record, "245", "1", NO_INDICATOR, "a");
            string titleRemainder = BookMapper.getFieldNRSubFieldValue(record, "245", "1", NO_INDICATOR, "b");

            Assert.AreEqual(
                $"{title} {titleRemainder}",
                result.Title, "Title should be mapped from indicated field and subfield for title and titleRemainder");
            Assert.AreEqual("Anthologies - Eric Carle's Dragons Dragons and Other Creatures Part II", result.Title);

            Assert.AreEqual(
                BookMapper.getFieldNRSubFieldValue(record, "260", NO_INDICATOR, NO_INDICATOR, "b"),
                result.PublisherName, "PublisherName should be mapped from indicated field and subfield");
            Assert.AreEqual("National Council of Teachers of English", result.PublisherName);

            Assert.AreEqual(
                BookMapper.getFieldNRSubFieldValue(record, "260", NO_INDICATOR, NO_INDICATOR, "c"),
                result.PublishedDate, "PublishedDate should be mapped from indicated field and subfield");
            Assert.AreEqual("Dec 1992", result.PublishedDate);

            Assert.AreEqual(
                BookMapper.getFieldNRSubFieldValue(record, "856", "4", "0", "u"),
                result.FullTextLink, "FullTextLink should be mapped from indicated field and subfield");
            Assert.AreEqual("https://search.proquest.com/docview/196889092/fulltextPDF/embedded/4HEPD3GZJWMLET69?source=fedsrch", result.FullTextLink);
        }
    }
}
