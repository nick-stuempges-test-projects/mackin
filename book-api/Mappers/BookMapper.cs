using System;
using book_api.Models;

namespace book_api.Mappers
{
    public static class BookMapper
    {
        private const string NO_INDICATOR = "";

        public static Book mapToBook(MARC21 record)
        {
            Book book = new Book();

            book.SystemControlNumber = getFieldNRSubFieldValue(record, "035", NO_INDICATOR, NO_INDICATOR, "a");
            string title = getFieldNRSubFieldValue(record, "245", "1", NO_INDICATOR, "a");
            string titleRemainder = getFieldNRSubFieldValue(record, "245", "1", NO_INDICATOR, "b");
            book.Title = $"{title}{(!string.IsNullOrWhiteSpace(titleRemainder) ? " ": "")}{titleRemainder}";
            // side-note https://www.loc.gov/marc/bibliographic/bd260.html
            // lets assume for the sake of the exercise that the json format forces subfield b (name of publisher)
            // and subfield c (date of publication) to be non repeating
            book.PublisherName = getFieldNRSubFieldValue(record, "260", NO_INDICATOR, NO_INDICATOR, "b");
            book.PublishedDate = getFieldNRSubFieldValue(record, "260", NO_INDICATOR, NO_INDICATOR, "c");
            book.FullTextLink = getFieldNRSubFieldValue(record, "856", "4", "0", "u");

            return book;
        }

        // given a 3 character field code, field indicators, and a subField tag
        // return the value of the found subfield, or null if not found
        // do not use with repeating subfields, only NR (non-repeating) subfields
        public static string getFieldNRSubFieldValue(
            MARC21 record,
            string fieldTag,
            string fieldIndicator1,
            string fieldIndicator2,
            string subFieldCode) 
        {
            if (record?.datafield == null || record.datafield.Count == 0) {
                return null;
            }

            Field matchedField = record.datafield.Find(field => (
                field.Tag != null &&
                field.Tag.Equals(fieldTag, StringComparison.InvariantCultureIgnoreCase) &&
                field.Indicator1.Equals(fieldIndicator1, StringComparison.InvariantCultureIgnoreCase) &&
                field.Indicator2.Equals(fieldIndicator2, StringComparison.InvariantCultureIgnoreCase)
            ));

            if (matchedField?.SubFields == null || matchedField.SubFields.Count == 0) {
                return null;
            }

            string rawValue = matchedField.SubFields.Find(
                subField => (subField.Code != null && subField.Code.Equals(subFieldCode, StringComparison.InvariantCultureIgnoreCase))
            )?.Text;
            
            return string.IsNullOrWhiteSpace(rawValue) ? null : rawValue.Trim();
        }
    }


}