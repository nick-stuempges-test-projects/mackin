using System.Collections.Generic;

namespace book_api.Models
{
    public class MARC21PostResponse
    {
        public List<string> messages { get; set; }
        public List<Result> results { get; set; }
    }

    public class Result
    {
        public string position { get; set; }
        public bool success { get; set; }
        public string systemControlNumber { get; set; }
        public List<string> messages { get; set; }
    }
}