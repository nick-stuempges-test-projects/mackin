using System.Collections.Generic;

namespace book_api.Models
{
    public class MARC21PostRequest
    {
        public string Version { get; set; }
        public int Count { get; set; }
        public List<MARC21> Records { get; set; }
    }
}