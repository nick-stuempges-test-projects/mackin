using System.ComponentModel.DataAnnotations;

namespace book_api.Models
{
    public class Book
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public string PublisherName  { get; set; }

        [Required]
        public string PublishedDate  { get; set; }

        [Required]
        public string FullTextLink  { get; set; }
        
        [Required]
        public string SystemControlNumber  { get; set; }
    }
}