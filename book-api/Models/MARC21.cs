using System.Collections.Generic;
using book_api.JsonConverters;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace book_api.Models
{
    public class MARC21
    {
        public string position { get; set; }
        public List<Field> datafield { get; set; }
    }

    public class Field
    {
        [JsonProperty("tag")]
        public string Tag { get; set; }
        
        [JsonProperty("indicator1")]
        public string Indicator1 { get; set; }

        [JsonProperty("indicator2")]
        public string Indicator2 { get; set; }
        
        [JsonProperty("subField")]
        [JsonConverter(typeof(SingleOrArrayConverter<SubField>))]
        public List<SubField> SubFields { get; set; }
    }

    public class SubField
    {
        public string Code { get; set; }
        public string Text { get; set; }
    }
}