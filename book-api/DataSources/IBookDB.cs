using System.Collections.Generic;
using System.Threading.Tasks;
using book_api.Models;
using Microsoft.AspNetCore.Mvc;

namespace book_api.DataSources
{
    public interface IBookDB
    {
        Task<List<Book>> GetAll();
        Task<Book> GetBySystemControlNumber(string systemControlNumber);
        Task Insert(Book book);
        Task Update(Book book);
        Task<bool> Upsert(Book book);

        // returns number of records affected
        Task<int> Delete(string systemControlNumber);
    }
}