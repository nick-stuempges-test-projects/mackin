using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using book_api.Models;

namespace book_api.DataSources
{
    public class BookDB : IBookDB
    {
        private Dictionary<string, Book> _bookContext;

        public BookDB()
        {
            _bookContext = new Dictionary<string, Book>();
        }

        public Task<List<Book>> GetAll()
        {
            return Task.FromResult(_bookContext.Values.ToList());
        }

        public Task<Book> GetBySystemControlNumber(string systemControlNumber)
        {
            Book result = null;
            if (_bookContext.ContainsKey(systemControlNumber))
            {
                result = _bookContext[systemControlNumber];
            }

            return Task.FromResult(result);
        }

        public Task Insert(Book book)
        {
            if (_bookContext.ContainsKey(book.SystemControlNumber))
            {
                throw new System.Exception("Attempted to insert a record that already exists");
            }

            _bookContext.Add(book.SystemControlNumber, book);

            return Task.CompletedTask;
        }

        public Task Update(Book book)
        {
            if (!_bookContext.ContainsKey(book.SystemControlNumber)) 
            {
                throw new System.Exception("Attempted to update a record that does not exist");
            }

            _bookContext[book.SystemControlNumber] = book;
            return Task.CompletedTask;
        }

        public Task<bool> Upsert(Book book)
        {
            if (!_bookContext.ContainsKey(book.SystemControlNumber)) 
            {
                _bookContext.Add(book.SystemControlNumber, book);
            } else {
                _bookContext[book.SystemControlNumber] = book;
            }

            return Task.FromResult<bool>(true);
        }

        public Task<int> Delete(string systemControlNumber)
        {
            if (_bookContext.ContainsKey(systemControlNumber)) {
                _bookContext.Remove(systemControlNumber);
                return Task.FromResult<int>(1);
            }

            return Task.FromResult<int>(0);
        }
    }
}