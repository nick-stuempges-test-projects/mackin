
using System.Threading.Tasks;
using book_api.Models;

namespace book_api.Services
{
    public interface IBookService
    {
        Task<MARC21PostResponse> PostMARC21(MARC21PostRequest request);
    }
}