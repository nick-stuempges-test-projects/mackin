using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using book_api.DataSources;
using book_api.Mappers;
using book_api.Models;

namespace book_api.Services
{
    public class BookService : IBookService
    {
        private readonly IBookDB _bookDB;
        public BookService(IBookDB bookDB)
        {
            _bookDB = bookDB;
        }

        public async Task<MARC21PostResponse> PostMARC21(MARC21PostRequest request)
        {
            MARC21PostResponse response = new MARC21PostResponse();
            response.messages = new List<string>();
            response.results = new List<Result>();

            Dictionary<string, Book> books = new Dictionary<string, Book>();

            foreach(MARC21 record in request.Records) {
                if (string.IsNullOrWhiteSpace(record.position)) {
                    response.messages.Add($"position missing on record, no records processed");
                    return response;
                }

                if (books.ContainsKey(record.position)) {
                    response.messages.Add($"duplicate position identifier in records ({record.position}), no records processed");
                    return response;
                }

                books.Add(record.position, BookMapper.mapToBook(record));
            }

            foreach(string position in books.Keys.ToList()) {
                Book book = books[position];

                ValidationContext validationContext = new ValidationContext(book, serviceProvider: null, items: null);
                List<ValidationResult> validationResults = new List<ValidationResult>();
                if (!Validator.TryValidateObject(book, validationContext, validationResults))
                {
                    Result bookResult = new Result();
                    bookResult.position = position;
                    bookResult.success = false;
                    bookResult.messages = validationResults.Select(validationResult => validationResult.ErrorMessage).ToList();
                    response.results.Add(bookResult);

                    books.Remove(position);
                }
            }

            // side-note there are some optimizations here depending on your database layer
            // batch save getting back a collection of DB results with an insert and an on conflict update
            // that's not the point here as we have a static collection as a DB, so inneficent and simple is fine for this
            foreach(string position in books.Keys) {
                Book book = books[position];
                Result bookResult = new Result();
                bookResult.position = position;
                bookResult.systemControlNumber = book.SystemControlNumber;

                bool success = await _bookDB.Upsert(book);
                bookResult.success = success;

                if(!success) {
                    // side-note whatever the issue was in the DB results - use the error messages here
                    bookResult.messages.Add("An unexpected database error occured");
                }

                response.results.Add(bookResult);
            }

            return response;
        }
    }
}