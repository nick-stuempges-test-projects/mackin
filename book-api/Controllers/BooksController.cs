﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using book_api.DataSources;
using book_api.Models;
using book_api.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace book_api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {
        private readonly ILogger<BooksController> _logger;
        private readonly IBookService _bookService;
        private readonly IBookDB _bookDB;

        public BooksController(ILogger<BooksController> logger, IBookService bookService, IBookDB bookDB)
        {
            _logger = logger;
            _bookService = bookService;
            _bookDB = bookDB;
        }

        [HttpGet]
        public async Task<List<Book>> GetAll()
        {
            return await _bookDB.GetAll();
        }

        [HttpGet("{systemControlNumber}")]
        public async Task<IActionResult> Get(string systemControlNumber)
        {
            Book result = await _bookDB.GetBySystemControlNumber(systemControlNumber);

            if (result == null) {
                return new NotFoundResult();
            }

            return new OkObjectResult(result);
        }

        [HttpPut("{systemControlNumber}")]
        public async Task<IActionResult> Put(string systemControlNumber, Book book)
        {
            if (!ModelState.IsValid || !String.Equals(systemControlNumber, book.SystemControlNumber))
            {
                return BadRequest("Invalid Request: systemConotrolNumber does match request body systemControlNumber");
            }

            Book existingBook = await _bookDB.GetBySystemControlNumber(book.SystemControlNumber);
            if (existingBook != null) {
                await _bookDB.Update(book);
                return Ok();
            } else {
                await _bookDB.Insert(book);
                return Created(systemControlNumber, book);
            }
        }

        [HttpDelete("{systemControlNumber}")]
        public async Task<IActionResult> Delete(string systemControlNumber)
        {
            int affectedRecords = await _bookDB.Delete(systemControlNumber);
            if (affectedRecords == 0) {
                return NotFound();
            }

            return Ok();
        }

        [HttpPost("MARC21")]
        public async Task<IActionResult> Post(MARC21PostRequest request)
        {
            MARC21PostResponse resp = await _bookService.PostMARC21(request);

            if (resp.messages != null && resp.messages.Count > 0) {
                return new UnprocessableEntityObjectResult(resp);
            }
               
            return new OkObjectResult(resp);
        }
    }
}   
